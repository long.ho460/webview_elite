import 'dart:async';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:web_view_elite/course_screen.dart';
import 'package:webview_cookie_manager/webview_cookie_manager.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart'
as in_app_web_view;
import 'login_page.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final Uri url = Uri.parse("https://devlearner.elitelearning.vn/#/lesson/course-v1:Horusoftaceae+APPSTORE_01+2021_T1/block-v1:Horusoftaceae+APPSTORE_01+2021_T1+type@chapter+block@1bf38d4834c94a2581d2fc4f346cf4ec");

  // final WebviewCookieManager cookieManager = WebviewCookieManager();
  in_app_web_view.CookieManager cookieManager = in_app_web_view.CookieManager.instance();

  // cookieManager.deleteAllCookies();


  var cookies = await cookieManager.getCookies(url: url);
  print(cookies.toString());
  print("expire cookie:");
  // print(cookies[0].domain);

  runApp(MaterialApp(
     home: cookies.isEmpty
         ? LoginPage()
         : CourseWidget())
  );

  // runApp(MaterialApp(
  //   home: LoginPage(),
  // ));
}
