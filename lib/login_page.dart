import 'dart:convert';
import 'dart:io';

import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:webview_cookie_manager/webview_cookie_manager.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart'
as in_app_web_view;
import 'account.dart';
import 'course_screen.dart';

class LoginPage extends StatefulWidget {
  static String tag = "login-page";

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  TextEditingController emailController = TextEditingController();
  TextEditingController passController = TextEditingController();

  // String email = "";
  // String password = "";


  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('assets/travel.png'),
      ),
    );

    var edtEmail = TextFormField(
      controller: emailController,
      // onChanged: (email) => setState(() => this.email = email),
      // onSaved: (input) => email = input!,
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(32.0),
        ),
      ),
    );

    final edtPassword = TextFormField(
      controller: passController,
      // onChanged: (password) => setState(() => this.password = password),
      // onSaved: (input) => password = input!,
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(32.0),
        ),
      ),
    );

    final loginButton = Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.lightBlueAccent.shade100,
        // elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () async {
            // get token
            var account =
                await login(context: context, username: emailController.text.toString(), password: passController.text.toString());
            print("Account: ");
            print(account.toJson());

            // var dio = Dio();
            // var cookieJar = CookieJar();
            // dio.interceptors.add(CookieManager(cookieJar));
            // await dio.post(
            //   'https://devcourses.elitelearning.vn/oauth2/login/',
            //   options: Options(
            //     headers: {
            //       HttpHeaders.authorizationHeader:
            //       'Bearer ' + account.access_token,
            //     },
            //   ),
            // );
            //
            // var cookies = await cookieJar.loadForRequest(
            //     Uri.parse('https://devcourses.elitelearning.vn/oauth2/login/'));

            // move to course screen
            // final WebviewCookieManager cookieManager = WebviewCookieManager();
            // cookieManager.clearCookies(); // clear cookie before move course screen (login -> create cookie)
            in_app_web_view.CookieManager cookieManager = in_app_web_view.CookieManager.instance();
            cookieManager.deleteAllCookies();
            Route route = MaterialPageRoute(builder: (context) => CourseWidget(token: account.access_token));
            Navigator.push(context, route);
          },
          color: Colors.lightBlueAccent,
          child: const Text(
            'Login',
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );

    final forgotLabel = TextButton(
        child:
        const Text('Forgot password?', style: TextStyle(color: Colors.black54)),
        onPressed: () {});

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: [
              logo,
              SizedBox(height: 40),
              edtEmail,
              SizedBox(height: 40),
              edtPassword,
              SizedBox(height: 40),
              loginButton,
              forgotLabel
            ],
          )),
    );
  }
}


Future<Account> login({required var context, required var username, required var password}) async {
  final response = await http.post(
    Uri.parse('https://devcourses.elitelearning.vn/oauth2/access_token/'),
    body: {
      'grant_type': 'password',
      'client_id': 'e10f0ece8e76d9f286e3',
      'username': username,
      'password': password
    },
  );
  if (response.statusCode == 200) {
    print('200 OK');
    // If the server did return a 200 CREATED response,
    // then parse the JSON.
    return Account.fromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 200 CREATED response,
    // then throw an exception.
    ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text("Failed to login"),)
    );
    throw Exception('Failed to login.');
  }
}