import 'dart:async';
import 'dart:io';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart' as dio_cookie;
import 'package:flutter/material.dart';

import 'package:flutter_inappwebview/flutter_inappwebview.dart'
    as in_app_web_view;
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:http/http.dart' as http;
import 'package:webview_cookie_manager/webview_cookie_manager.dart';
// import 'dart:_http' as http_cookie;

class CourseWidget extends StatefulWidget {
  CourseWidget({Key? key, this.token}) : super(key: key);
  // final List<Cookie> cookies;
  var token; // null if from main, else from login

  @override
  _CourseWidgetState createState() => _CourseWidgetState();
}

class _CourseWidgetState extends State<CourseWidget> {
  late in_app_web_view.InAppWebViewController _webViewController;
  // final WebviewCookieManager cookieManager = WebviewCookieManager();
  in_app_web_view.CookieManager cookieManager = in_app_web_view.CookieManager.instance();

  var domain = '.elitelearning.vn';
  String url =
      "https://devlearner.elitelearning.vn/#/lesson/course-v1:Horusoftaceae+APPSTORE_01+2021_T1/block-v1:Horusoftaceae+APPSTORE_01+2021_T1+type@chapter+block@1bf38d4834c94a2581d2fc4f346cf4ec";
  double progress = 0;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('InAppWebView Example'),
      ),
      body: Container(
        child: Column(children: <Widget>[
          Container(
            padding: const EdgeInsets.all(10.0),
            child: Text((url.length > 50) ? url.substring(0, 50) + "..." : url),
          ),
          Container(
              padding: const EdgeInsets.all(10.0),
              child: progress < 1.0
                  ? LinearProgressIndicator(value: progress)
                  : Container()),
          Expanded(
            child: Container(
              margin: const EdgeInsets.all(10.0),
              decoration:
                  BoxDecoration(border: Border.all(color: Colors.blueAccent)),
              child: in_app_web_view.InAppWebView(
                initialUrlRequest:
                    in_app_web_view.URLRequest(url: Uri.parse(url)),
                onWebViewCreated:
                    (in_app_web_view.InAppWebViewController controller) async {
                  _webViewController = controller;

                  // get cookies or create
                  var cookiesJS;
                  if (widget.token == null) {
                    // get from cookieManager
                    print("--- has cookie ---");
                    return;
                  }
                  // get from dio.post
                  print("--- has not cookie ---" + widget.token);
                  var dio = Dio();
                  var cookieJar = CookieJar();
                  dio.interceptors.add(dio_cookie.CookieManager(cookieJar));
                  await dio.post(
                    'https://devcourses.elitelearning.vn/oauth2/login/',
                    options: Options(
                      headers: {
                        HttpHeaders.authorizationHeader:
                            'Bearer ' + widget.token,
                      },
                    ),
                  );

                  cookiesJS = await cookieJar.loadForRequest(Uri.parse(
                      'https://devcourses.elitelearning.vn/oauth2/login/'));
                  print("---cookiesJS---");
                  print(cookiesJS.toString());

                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text(cookiesJS.toString()),
                  ));

                  // tạo cookies -> save
                  // await cookieManager.clearCookies();

                  // parse cookiesJS -> cookies 3 field quan trọng & expire
                  final Map<String, String> cookies = {};

                  for (var i = 0; i < cookiesJS.length; i++) {
                    print("Cookie $i");
                    print(cookiesJS[i].name + ": " + cookiesJS[i].value);
                    if (cookiesJS[i].name == 'csrftoken') {
                      cookies['csrftoken'] = cookiesJS[i].value;
                    }
                    if (cookiesJS[i].name == 'openedx-language-preference') {
                      cookies['openedx-language-preference'] =
                          cookiesJS[i].value;
                    }
                    if (cookiesJS[i].name == 'sessionid') {
                      cookies['sessionid'] = cookiesJS[i].value;
                    }
                  }

                  // ScaffoldMessenger.of(context).showSnackBar(
                  //     SnackBar(content: Text((await cookieManager.getCookies(url)).toString()),)
                  // );
                  // await cookieManager.setCookies(widget.cookies);

                  print("cookie is; " + cookies.toString());
                  print("cookiesJS expire: ");
                  print(cookiesJS[0].expires.millisecondsSinceEpoch);

/*                  await cookieManager.setCookies(cookies.entries
                      .map((item) => Cookie(item.key, item.value)
                        ..domain = domain
                        ..expires = cookiesJS[0].expires
                        ..httpOnly = false)
                      .toList());*/

                  // set the cookie
                  await cookieManager.setCookie(
                    url: Uri.parse(url),
                    name: 'csrftoken',
                    value: cookies['csrftoken'].toString(),
                    domain: domain,
                    expiresDate: cookiesJS[0].expires.millisecondsSinceEpoch,
                    isHttpOnly: false,
                  );

                  await cookieManager.setCookie(
                    url: Uri.parse(url),
                    name: 'openedx-language-preference',
                    value: cookies['openedx-language-preference'].toString(),
                    domain: domain,
                    expiresDate: cookiesJS[0].expires.millisecondsSinceEpoch,
                    isHttpOnly: false,
                  );

                  await cookieManager.setCookie(
                    url: Uri.parse(url),
                    name: 'sessionid',
                    value: cookies['sessionid'].toString(),
                    domain: domain,
                    expiresDate: DateTime.now().add(const Duration(days: 10)).millisecondsSinceEpoch,
                    isHttpOnly: false,
                  );


                  var cocky = await cookieManager.getCookies(url: Uri.parse('https://devlearner.elitelearning.vn/#/lesson/course-v1:Horusoftaceae+APPSTORE_01+2021_T1/block-v1:Horusoftaceae+APPSTORE_01+2021_T1+type@chapter+block@1bf38d4834c94a2581d2fc4f346cf4ec'));
                  print("cocky: ");
                  print(cocky.toString());
                    },
                onLoadStart: (in_app_web_view.InAppWebViewController controller,
                    Uri? uri) {
                  setState(() {
                    this.url = uri!.host;
                  });
                },
                onLoadStop: (in_app_web_view.InAppWebViewController controller,
                    Uri? uri) async {
                  setState(() {
                    this.url = uri!.host;
                  });
                },
                onProgressChanged:
                    (in_app_web_view.InAppWebViewController controller,
                        int progress) {
                  setState(() {
                    this.progress = progress / 100;
                  });
                },
                onLoadHttpError: (in_app_web_view.InAppWebViewController controller, Uri? url,
                int statusCode, String description) {
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: Text("Load error"),
                  ));
                },
                onLoadError: (in_app_web_view.InAppWebViewController controller, Uri? url, int code,
                String message){
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: Text("Load error"),
                  ));
                },
                androidOnReceivedLoginRequest: (InAppWebViewController controller, LoginRequest loginRequest){
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: Text("Load error"),
                  ));
                },

              ),
            ),
          ),
          ButtonBar(
            alignment: MainAxisAlignment.center,
            children: <Widget>[
              MaterialButton(
                child: const Icon(Icons.arrow_back),
                onPressed: () {
                  _webViewController.goBack();
                },
              ),
              MaterialButton(
                child: const Icon(Icons.arrow_forward),
                onPressed: () {
                  _webViewController.goForward();
                },
              ),
              MaterialButton(
                child: const Icon(Icons.refresh),
                onPressed: () {
                  _webViewController.reload();
                },
              ),
              MaterialButton(
                child: const Icon(Icons.error),
                onPressed: () async {
                  await cookieManager.setCookie(
                    url: Uri.parse(url),
                    name: 'sessionid',
                    value: 'fakecookie',
                    domain: domain,
                    isHttpOnly: false,
                  );


                  var cookies = await cookieManager.getCookies(url: Uri.parse('https://devlearner.elitelearning.vn/#/lesson/course-v1:Horusoftaceae+APPSTORE_01+2021_T1/block-v1:Horusoftaceae+APPSTORE_01+2021_T1+type@chapter+block@1bf38d4834c94a2581d2fc4f346cf4ec'));
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text(cookies.toString()),
                  ));
                },
              ),
            ],
          ),
        ]),
      ),
    );
  }
}
