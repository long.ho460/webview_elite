class Account {
  var access_token;
  var token_type;
  var expires_in;
  var refresh_token;
  var scope;


  Account({
    this.access_token,
    this.token_type,
    this.expires_in,
    this.refresh_token,
    this.scope,
  });

  factory Account.fromJson(Map<String, dynamic> json) {
    return Account(
      access_token: json['access_token'],
      token_type: json['token_type'],
      expires_in: json['expires_in'],
      refresh_token: json['refresh_token'],
      scope: json['scope'],
    );
  }

  Map<String, dynamic> toJson() => {
    'access_token': access_token,
    'token_type': token_type,
    'expires_in': expires_in,
    'refresh_token': refresh_token,
    'scope': scope,
  };

}
